document.addEventListener('DOMContentLoaded', function() {

    // Letter Picker
    const source = document.getElementById('letterinput');
    const result = document.getElementById('letteroutput');
    const inputHandler = function(e) {
        result.innerText = e.target.value.substring(0,1).toUpperCase();
    }
    source.addEventListener('input', inputHandler);
    source.addEventListener('propertychange', inputHandler); // for IE8

    // Color Picker
    function getColor(e)
    {
        document.getElementById("example_div").style.backgroundColor = document.getElementById("colorpicker").value;
    }
    const colorsource = document.getElementById('colorpicker');
    colorsource.addEventListener('input', getColor)

    // Account Selector Setup
    chrome.storage.local.get(["computerList"], function(items){
        try {
            let list = items["computerList"];
            for (var key in list) {
                extras_select = document.getElementById("extras")
                var opt = document.createElement('option');
                opt.value = list[key].issuer;
                opt.innerText = list[key].issuer;
                extras_select.appendChild(opt);
            }
        } catch (e) {
            console.log("THERES AN ERROR. ID: ES1");
        }
    });

    // Confirm Button
    var confirm_button = document.getElementById('confirm_button');
    confirm_button.addEventListener('click', function() {
        chrome.storage.local.get(["extras"], function(items){
            items = items["extras"]
            // Setup items as list if string
            if (!items || typeof(items) != typeof([])) {items = [];}
            // Append item to list of items
            items.push([document.getElementById("colorpicker").value, document.getElementById("letterinput").value.substring(0,1).toUpperCase(), document.getElementById("extras").value])
            // Push Items to storage
            chrome.storage.local.set({ "extras": items}, function(){});

            window.location.href = '/post.html';
        })
    }, false);

}, false);

